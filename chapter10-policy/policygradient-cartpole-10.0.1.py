# this codes is from
# https://github.com/karnigili/ReinforcementLearning/blob/master/Tutorials/REINFORCE.ipynb

import gym
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

import tensorflow as tf
from tensorflow.python.keras.layers import Dense, Dropout
from tensorflow.python.keras import Sequential
from tensorflow.keras.optimizers import Adam
from tensorflow.python.keras.models import load_model

## Config ##
ENV="CartPole-v1"
RANDOM_SEED=1
N_EPISODES=1000

# random seed (reproduciblity)
np.random.seed(RANDOM_SEED)
tf.random.set_seed(RANDOM_SEED)

# set the env
env=gym.make(ENV) # env to import
env.seed(RANDOM_SEED)
env.reset() # reset to env

class REINFORCE:
  def __init__(self, env, path=None):
    self.env=env #import env
    self.state_shape=env.observation_space.shape # the state space
    self.action_shape=env.action_space.n # the action space
    self.gamma=0.99 # decay rate of past observations
    self.alpha=1e-3 # learning rate in the policy gradient
    self.learning_rate=0.01 # learning rate in deep learning

    if not path:
      self.model=self._create_model() #build model
    else:
      self.model=self.load_model(path) #import model

    # record observations
    self.total_rewards=[]
    self.memory = []

  def _create_model(self):
    ''' builds the model using keras'''
    model=Sequential()

    # input shape is of observations
    model.add(Dense(24, input_shape=self.state_shape, activation="relu"))
    #model.add(Dropout(0.5))
    # introduce a relu layer
    model.add(Dense(12, activation="relu"))
    #model.add(Dropout(0.5))

    # output shape is according to the number of action
    # The softmax function outputs a probability distribution over the actions
    model.add(Dense(self.action_shape, activation="softmax"))
    model.compile(loss="categorical_crossentropy",
            optimizer=Adam(lr=self.learning_rate))

    return model

  def remember(self, state, action, action_prob, reward):
    '''stores observations'''
    self.memory.append ((state, action, action_prob, reward))

  def get_action(self, state):
    '''samples the next action based on the policy probabilty distribution
      of the actions'''

    # transform state
    state=state.reshape([1, state.shape[0]])
    # get action probably
    action_probability_distribution=self.model.predict(state).flatten()

    # sample action
    action=np.random.choice(self.action_shape,1,
                            p=action_probability_distribution)[0]

    return action, action_probability_distribution


  def get_discounted_rewards(self, rewards):
    '''Use gamma to calculate the total reward discounting for rewards
    Following - \gamma ^ t * Gt'''

    discounted_rewards=[]
    cumulative_total_return=0
    # iterate the rewards backwards and and calc the total return
    for reward in rewards[::-1]:
      cumulative_total_return = (cumulative_total_return*self.gamma)+reward
      discounted_rewards.insert(0, cumulative_total_return)

    # standardize discounted rewards
    mean_rewards=np.mean(discounted_rewards)
    std_rewards=np.std(discounted_rewards) + 1e-7 # avoiding zero div
    return (discounted_rewards-mean_rewards) / std_rewards


  def update_policy(self):
    '''Updates the policy network using the NN model.
    This function is used after the MC sampling is done - following
    \delta \theta = \alpha * gradient + log pi'''

    train_xs, train_ys = [], []

    discounted_rewards = self.get_discounted_rewards([ step [3] for step in self.memory ])
    for idx, (state, action, action_prob, _) in enumerate (self.memory):
        train_xs.append (state)

        # get y
        encoded_action = np.zeros(self.action_shape)
        encoded_action [action] = 1.
        gradient = (encoded_action - action_prob) * discounted_rewards [idx]
        y = action_prob + (self.alpha * gradient)
        # print (discounted_rewards [idx], action_prob, gradient, y)
        train_ys.append (y)

    history=self.model.train_on_batch(np.array (train_xs), np.array (train_ys))

    self.memory = []
    return history


  def train(self, episodes, rollout_n=1, render_n=50):
    '''train the model
        episodes - number of training iterations
        rollout_n- number of episodes between policy update
        render_n - number of episodes between env rendering '''

    env=self.env
    total_rewards=np.zeros(episodes)

    for episode in range(episodes):
      # each episode is a new game env
      state=env.reset()
      done=False
      episode_reward=0 #record episode reward

      while not done:
        # play an action and record the game state & reward per episode
        action, prob=self.get_action(state)
        next_state, reward, done, _=env.step(action)
        self.remember(state, action, prob, reward)
        state=next_state
        episode_reward+=reward

        #if episode%render_n==0: ## render env to visualize.
          #env.render()
        if done:
          # update policy
          if episode%rollout_n==0:
            self.update_policy()

      total_rewards[episode]=episode_reward
      print ('episode', episode, 'episode_reward:', episode_reward)

    self.total_rewards=total_rewards

  def save_model(self):
    '''saves the moodel // do after training'''
    self.model.save('REINFORCE_model.h5')

  def load_model(self, path):
    '''loads a trained model from path'''
    return load_model(path)


if __name__ == '__main__':
    agent=REINFORCE(env)
    agent.train(500, 1)

